package com.threealike.life.reflection;

public class Classes {
	public static final Class<?> forName(String classSig) {
		try {
			return Class.forName(classSig);
		} catch (java.lang.ClassNotFoundException e) {
			throw new ReflectionException(e);
		}
	}
	public static final Object newInstance(Class<?> c) {
		try {
			return c.newInstance();
		} catch (InstantiationException e) {
			throw new ReflectionException(e);
		} catch (IllegalAccessException e) {
			throw new ReflectionException(e);
		}
	}
}
