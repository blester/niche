package com.threealike.life.reflection;

public class ReflectionException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ReflectionException() {
	}

	public ReflectionException(String message) {
		super(message);
	}

	public ReflectionException(Throwable cause) {
		super(cause);
	}

	public ReflectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
