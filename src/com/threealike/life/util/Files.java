package com.threealike.life.util;

import java.io.*;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Helps make java.io.File suck less
 * 
 * @author Brizert
 *
 */
public class Files {

    public static boolean equal(File f1, File f2) {
        return canonicalPath(f1).equals(canonicalPath(f2));
    }

	public static String canonicalPath(File f) {
		try {
			return f.getCanonicalPath();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}

    public static String relativePath(File parent, File child) {
        String parentPath = canonicalPath(parent);
        String childPath = canonicalPath(child);
        return childPath.substring(parentPath.length()+1);
    }
	
	public static void transfer(InputStream is, File file) {
		
		FileOutputStream os = fileOS(file);
		
		byte[] buf = new byte[1024];
		int len;
		try {
			while((len = is.read(buf)) != -1)
				os.write(buf, 0, len);
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		} finally {
			try {
				os.close();
				is.close();
			} catch (IOException e) {
				//
			}
		}
		
	}
	
	public static boolean create(File f) {
		try {
			return f.createNewFile();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static FileOutputStream fileOS(File file) {
		try {
			return new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static void closeIS(InputStream is) {
		if(is == null) return;
		try {
			is.close();
		} catch(IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static void closeOS(OutputStream os) {
		if(os == null) return;
		try {
			os.close();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static FileInputStream fileIS(File file) {
		try {
			return new FileInputStream(file);
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}

    public static String getContents(String f) {
        return getContents(new File(f));
    }

    public static String getContents(File f) {
        StringBuilder sb = new StringBuilder();
        InputStreamReader rdr = new InputStreamReader(fileIS(f));
        int len;
        char[] buf = new char[1024];
        while((len = read(rdr, buf)) != -1) {
            sb.append(new String(buf, 0, len));
        }
        return sb.toString();
    }

    public static FileWriter fileWriter(File f) {
        return fileWriter(f, false);
    }

    public static FileWriter fileWriter(File f, boolean append) {
        try {
            return new FileWriter(f, append);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(Writer writer, String content) {
        try {
            writer.write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void append(Writer writer, String content) {
        try {
            writer.append(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void close(Writer writer) {
        try {
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void append(File f, String content) {
        FileWriter writer = fileWriter(f, true);
        append(writer, content);
        close(writer);
    }

    public static String md5Of(File f) {
        return hashOf(f, "MD5");
    }

    public static String sha256Of(File f) {
        return hashOf(f, "SHA-256");
    }

    public static String hashOf(File f, String algo) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        DigestInputStream dis = new DigestInputStream(fileIS(f), md);
        byte[] buf = new byte[1024];
        while(read(dis, buf) != -1) ;
        byte[] digest = md.digest();

        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        while(hashtext.length() < 32) {
            hashtext = "0"+hashtext;
        }

        return hashtext;
    }

    private static int read(InputStream is, byte[] buf) {
        try {
            return is.read(buf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static int read(Reader rdr, char[] buf) {
        try {
            return rdr.read(buf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static File lastCommonAncestor(File... f) {
        File out = lastCommonAncestor(f[0], f[1]);
        for(int i=2; i < f.length; i++)
            out = lastCommonAncestor(out, f[i]);
        return out;
    }

    /**
     * Given two files, finds last directory they have in common or null if they have no containing directories in
     * common.
     */
    public static File lastCommonAncestor(File f1, File f2) {
        File[] a1 = ancestors(f1);
        File[] a2 = ancestors(f2);

        File out = null;

        int min = a1.length > a2.length ? a2.length : a1.length;
        for(int i=0; i < min; i++) {
            String p1 = Files.canonicalPath(a1[i]);
            String p2 = Files.canonicalPath(a2[i]);
            if(p1.equals(p2)) {
                out = a1[i];
            } else if(out != null) break;
        }

        return out;
    }

    public static boolean isDescendantOf(File parent, File child) {
        String p = canonicalPath(parent);
        File c = child.getParentFile();
        while(c != null) {
            if(canonicalPath(c).equals(p)) return true;
            c = c.getParentFile();
        }
        return false;
    }

    public static File lastCommonAncestor(String... f) {
        File out = lastCommonAncestor(f[0], f[1]);
        for(int i=2; i < f.length; i++)
            out = lastCommonAncestor(new File(f[i]), out);
        return out;
    }

    public static File lastCommonAncestor(String f1, String f2) {
        return lastCommonAncestor(new File(f1), new File(f2));
    }

    /**
     * Given files, parent and child, returns the first file or folder that is the first-child of parent
     */
    public static File firstChild(File parent, File child) {
        String p = Files.canonicalPath(parent);
        do {
            if(p.equals(Files.canonicalPath(child.getParentFile())))
                return child;
            child = child.getParentFile();
        } while(child != null);
        return null;
    }

//    /**
//     * Given two files, finds the first two directories where their lineages diverge or null if they don't have any
//     * containing directories different from the other.
//     */
//    public static File[] firstUncommonAncestors(File f1, File f2) {
//        File[] a1 = ancestors(f1);
//        File[] a2 = ancestors(f2);
//
//        int max = a1.length > a2.length ? a1.length : a2.length;
//        for(int i=0; i < max; i++) {
//            String p1 = i < a1.length ? Files.canonicalPath(a1[i]) : null;
//            String p2 = i < a2.length ? Files.canonicalPath(a2[i]) : null;
////            System.out.println(p1+"\n"+p2+";");
//            if(p1 != null && p2 != null && p1.equals(p2)) {
//                continue;
//            }
//
//            return new File[] {p1 != null ? a1[i] : null, p2 != null ? a2[i] : null};
//        }
//
//        return null;
//    }

    public static File[] ancestors(File f) {
        List<File> out = new ArrayList<>();
        if(!f.isDirectory())
            f = f.getParentFile();
        out.add(f);
        while(f.getParentFile() != null) {
            out.add(f=f.getParentFile());
        }
        File[] ret = new File[out.size()];
        for(int i=0; i < out.size(); i++) {
            ret[i] = out.get(out.size()-i-1);
        }
        return ret;
    }
	
}
