package com.threealike.life.util;

import java.io.IOException;
import java.io.InputStream;

public class Parsers {
	
	public static final boolean isWhitespace(int ch) {
		return isWhitespace((char)ch);
	}
	
	public static final boolean isWhitespace(char ch) {
		switch(ch) {
		case cNEWLINE:
		case cTAB:
		case cRETURN:
		case cSPACE:
			return true;
		}
		return false;
	}
	
	public static final boolean isLineDelimiter(int ch) {
		return isLineDelimiter((char)ch);
	}
	
	public static final boolean isLineDelimiter(char ch) {
		switch(ch) {
		case cNEWLINE:
		case cRETURN:
			return true;
		}
		return false;
	}
	
	public static final byte next(InputStream source) throws IOException, EOFException {
		int out = source.read();
		if(out == -1) throw new EOFException();
		return (byte)out;
	}
	
	
	public static final boolean lookAhead(InputStream source, char[] chars) throws IOException, EOFException {
		for(int i=0; i < chars.length; i++)
			if(next(source) != chars[i])
				return false;
		return true;
	}
	
	public static final byte nextClean(InputStream source) throws IOException, EOFException {
		byte out;
		while(isWhitespace((out=next(source)))) ;
		return out;
	}
	
	
	@SuppressWarnings("serial")
	public static final class EOFException extends RuntimeException {}
	
	private static final char cNEWLINE = '\n';
	private static final char cTAB = '\t';
	private static final char cRETURN = '\r';
	private static final char cSPACE = ' ';
}
