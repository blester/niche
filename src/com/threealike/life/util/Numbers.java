package com.threealike.life.util;

public class Numbers {
	public static final int parseInt(String str) {
		try {
			return Integer.parseInt(Strings.nullString(str));
		} catch (NumberFormatException e) {
			return 0;
		}
	}
}
