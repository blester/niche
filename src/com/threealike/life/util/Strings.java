package com.threealike.life.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Strings
{
	public static final String NULLSTRING = "";
	
	public static final String unCamelize(String str)
	{
		byte[] orig = str.getBytes();
		byte[] lower = str.toLowerCase().getBytes();
		
		String out = "";
		int lastIndex = 0;
		for(int i=0; i < orig.length; i++)
		{
			if(orig[i] != lower[i] && i > 0)
			{
				out += new String(lower,lastIndex,i-lastIndex)+"_"+new String(lower,i,1);
				lastIndex = i+1;
			}
			if(i == orig.length - 1)
			{
				out += new String(lower,lastIndex,orig.length-lastIndex);
			}
		}
		return out;
	}
	
	public static final String camelize(String name)
	{
		//name = name.replace('_', '.');
		String[] parts = (name.toLowerCase()).split("_");
		String out = "";
		for(int i=0; i < parts.length; i++) {
			if(parts[i].length() < 1) continue;
			String fl = parts[i].substring(0,1);
			String rest = parts[i].substring(1,parts[i].length());
			if(!fl.equals((fl=fl.toUpperCase()))) {
				fl = fl.toUpperCase();
				rest = rest.toLowerCase();
				out = out.concat(fl.concat(rest));
			} else
				out = out.concat("_").concat(fl).concat(rest);
		}
		return out;
	}
	
	public static final boolean isEmpty(String str) {
		if(str == null) return true;
		if(str.trim().equals(NULLSTRING)) return true;
		return false;
	}
	
	public static final String nullString(String str) {
		return isEmpty(str) ? NULLSTRING : str;
	}
	
	public static final int countMatches(String haystack, String needle) {
		
		int out = 0;
		int anchor = 0;
		int index;
		while((index=haystack.indexOf(needle, anchor)) != -1) {
			out++;
			anchor = index+needle.length();
		}
		
		return out;
	}

    public static String sha256Of(String plaintext) {
        return hashOf(plaintext, "SHA-256");
    }

    public static String md5Of(String plaintext) {
        return hashOf(plaintext, "MD5");
    }

    public static String hashOf(String plaintext, String algo) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        m.reset();
        m.update(plaintext.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1,digest);
        String hashtext = bigInt.toString(16);

        while(hashtext.length() < 32 ){
            hashtext = "0"+hashtext;
        }

        return hashtext;
    }
}
