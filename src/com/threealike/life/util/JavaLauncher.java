package com.threealike.life.util;

import java.io.File;
import java.io.IOException;

import com.threealike.life.Cmd;
import com.threealike.life.file.Put;


public class JavaLauncher {
	
	private final String javaHome;
	private final String classpath;
	private final String classname;
	private final String args;
	
	public JavaLauncher(
			String javaHome,
			String classpath,
			String classname, 
			String[] args) {
		
		this.javaHome = javaHome;
		this.classpath = classpath;
		this.classname = classname;
		
		String argz = "";
		if(args != null)
			for(String arg : args)
				argz += arg+" ";
		
		this.args = argz;
	}
	
	/**
	 * Launches asynchronously
	 */
	public final void launch() {
		doLaunch(null, null);
	}
	
	/**
	 * Launches and waits for output (if both arguments are not null)
	 * 
	 * @param outputFile
	 * @param errorFile
	 */
	public final void launch(File outputFile, File errorFile) {
		doLaunch(outputFile, errorFile);
	}
	
	public final void launch(String outputFile, String errorFile) {
		doLaunch(new File(outputFile), new File(errorFile));
	}
	
	private final void doLaunch(File outputFile, File errorFile) {
		
		String java = new File(javaHome, "bin/java").getAbsolutePath();
		
		boolean asynch = false;
		String extra = "";
		if(outputFile != null && errorFile != null) {
			extra = " > "+outputFile.getAbsolutePath()+" 2> "+errorFile.getAbsolutePath();
			Files.create(outputFile);
			Files.create(errorFile);
			asynch = true;
		}
		
		String[] cmd = null;
		if(getOS().equals(OS.win)) {
			
			String cmdString = "@echo off \r\n\r\n \""+java+"\" -cp \""+classpath.replaceAll("\\\\", "\\\\\\\\")+"\" "+this.args+" "+classname+extra;
			File wincmd = new File("./lnch.cmd");
			Put.contents(wincmd, cmdString);
			cmd = new String[]{"lnch.cmd"};
			
		} else {
			/* assume Unix */
			cmd = new String[]{ "bash", "-c", java+" -cp "+classpath+" "+this.args+" "+classname+" "+extra };
		}
		
		
		if(asynch) {
			try {
				Runtime.getRuntime().exec(cmd);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			return;
		}
		
		
		
		Cmd.Results results = Cmd.exec(cmd);
		
		if(results.hasErr()) {
			//System.err.println("Launch Error:");
			System.err.print(results.err);
		}
		if(results.hasOut()) {
			//System.out.println("Launch Output:");
			System.out.print(results.out);
		}
		
	}
	
	
	public static final OS getOS()
	{
		if(System.getProperty("os.name").toUpperCase().matches("^WINDOWS.*"))
			return OS.win;
		return OS.unix;
	}
	public static enum OS {
		unix, win
	}
	

}
