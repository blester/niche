package com.threealike.life.util;

import java.io.File;

public class Arrays {
	
	public static final String[] merge(String[] ar1, String[] ar2) {
		
		String[] out = new String[ar1.length + ar2.length];
		
		int i=0;
		for(; i < ar1.length; i++)
			out[i] = ar1[i];
		for(int j=0; j < ar2.length; j++) {
			out[i] = ar2[j];
			i++;
		}
		
		return out;
	}
	
	public static final boolean contains(String[] array, String value)
	{
		for(String s : array) {
			if(s.equals(value))
				return true;
		}
		return false;
	}
	
	public static final boolean contains(Object[] arr, Object value)
	{
		for(Object o : arr)
		{
			if(o.equals(value))
				return true;
		}
		return false;
	}
	
	public static String[][] split(String[] array, int splitIndex)
	{
		splitIndex++;
		String[] out0 = new String[splitIndex];
		String[] out1 = new String[array.length-splitIndex];
		
		for(int i=0; i < out0.length; i++) {
			out0[i] = array[i];
		}
		for(int i=0; i < out1.length; i++) {
			out1[i] = array[splitIndex+i];
		}
		
		return new String[][]{out0,out1};
	}
	
	public static final void print(Object[] arr) {
		System.out.println(getString(arr));
	}
	
	public static final String getString(Object[] arr) {
		String out = "[";
		for(int i=0; i < arr.length; i++) {
			out += arr[i];
			if(i < arr.length-1)
				out += ", ";
		}
		out += "]";
		return out;
	}
	
	public static final String[] push(String[] source, String newval) {
		Object[] out = push(source, newval, new String[source.length+1]);
		return (String[])out;
	}
	
	public static final File[] push(File[] source, File newval) {
		Object[] out = push(source, newval, new File[source.length+1]);
		return (File[])out;
	}
	
	private static final Object[] push(Object[] source, Object newval, Object[] destination) {
		Object[] out = destination;
		for(int i=0; i < source.length; i++) {
			out[i] = source[i];
		}
		out[out.length-1] = newval;
		return out;
	}
	
	public static final File[] unshift(File[] source, File newval) {
		Object[] out = unshift(source, newval, new File[source.length+1]);
		return (File[])out;
	}
	
	private static final Object[] unshift(Object[] source, Object newval, Object[] destination) {
		Object[] out = destination;
		for(int i=0; i < source.length; i++) {
			out[i+1] = source[i];
		}
		out[0] = newval;
		return out;
	}
	
	public static final boolean equals(byte[] a1, int a1Offset, int a1Len, byte[] a2) {
		if(a1Len != a2.length) 
			return false;
		for(int i=0; i < a1Len; i++)
			if(a1[a1Offset+i] != a2[i]) 
				return false;
		return true;
	}
	
	//private static final Object
}
