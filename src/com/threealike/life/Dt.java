package com.threealike.life;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Dt {
	
	//private static final SimpleDateFormat sdf = new SimpleDateFormat();
	
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    public static final String ISO_8601_SIMPLE_DATE_PATTERN = "yyyyMMdd";
    public static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";
	public static final String DEFAULT_DATETIME_PATTERN = DEFAULT_DATE_PATTERN+" "+DEFAULT_TIME_PATTERN;
	public static final String ISO_8601_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String HTTP_DATETIME_PATTERN = "E, dd MMM yyyy HH:mm:ss z";
    public static final String ISO_8601_SIMPLE_DATETIME_PATTERN = "yyyMMdd'T'HHmmss'Z'";
	//private static final Calendar CAL = Calendar.getInstance();
	private static final TimeZone SYSTEM_TIMEZONE = Calendar.getInstance().getTimeZone();
	
	private static SimpleDateFormat sdf() {
		return new SimpleDateFormat();
	}
	
	public static String now()
	{
		return now(DEFAULT_DATE_PATTERN+" "+DEFAULT_TIME_PATTERN);
	}
	
	public static String now(String pattern)
	{
		SimpleDateFormat sdf = sdf();
		sdf.applyPattern(pattern);
		return sdf.format(new Date());
	}
	
	public static String now(String pattern, TZ timeZone)
	{
		return dateString(new Date(), pattern, timeZone);
	}
	
	public static String dateString(Date dt)
	{
		return dateString(dt, DEFAULT_DATETIME_PATTERN);
	}
	
	public static String dateString(Date dt, String pattern)
	{
		SimpleDateFormat sdf = sdf();
		sdf.applyPattern(pattern);
		return sdf.format(dt);
	}
	
	public static String dateString(Date dt, String pattern, TZ timeZone) {
		SimpleDateFormat sdf1 = sdf();
		sdf1.setTimeZone(timeZone.tz);
		sdf1.applyPattern(pattern);
		return sdf1.format(dt);
	}

	public static String dateString(Date dt, TZ timeZone) {
		return dateString(dt, Dt.DEFAULT_DATETIME_PATTERN, timeZone);
	}
	
	public static Date date(String dateString)
	{
		return date(DEFAULT_DATETIME_PATTERN, dateString);
	}
	
	public static Date date(String pattern, String dateString, TZ timeZone) {
		SimpleDateFormat sdf = sdf();
		sdf.applyPattern(pattern);
		if(timeZone != null)
			sdf.setTimeZone(timeZone.tz);
		try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static Date date(String pattern, String dateString)
	{
		return date(pattern, dateString, null);
	}

	public static Date date(String dateString, TZ tz) {
		return date(DEFAULT_DATETIME_PATTERN, dateString, tz);
	}

	public static Date date(Date dt, TZ tz) {
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		return CAL.getTime();
	}

	public static Date subtractDays(Date dt, int days) {
		return addDays(dt, -days);
	}

	public static Date addDays(Date dt, int days) {
		return new Date(dt.getTime() + (1000L*60L*60L*24L)*((long)days));
	}

	public static int day(Date dt)
	{
		return day(dt, TZ.SYSTEM);
	}

	public static int day(Date dt, TZ tz) {
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		return CAL.get(Calendar.DAY_OF_MONTH);
	}

	/* 1 = Sunday ... 7 = Saturday */
	public static int dayOfWeek(Date dt)
	{
		return dayOfWeek(dt, TZ.SYSTEM);
	}

	public static int dayOfWeek(Date dt, TZ tz)
	{
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		return CAL.get(Calendar.DAY_OF_WEEK);
	}

	/* remember month is zero-based (i.e. July is 6 instead of 7) */
	public static int month(Date dt)
	{
		return month(dt, TZ.SYSTEM);
	}

	public static int month(Date dt, TZ tz)
	{
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		return CAL.get(Calendar.MONTH);
	}

	public static int year(Date dt) {
		return year(dt, TZ.SYSTEM);
	}

	public static int year(Date dt, TZ tz) {
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		return CAL.get(Calendar.YEAR);
	}

	/**
	 * Rounds the given date down to the nearest day.
	 * (i.e. 2002-06-01 01:22:58 would become 2002-06-01 00:00:00)
	 */
	public static Date floor(Date dt) {
		return floor(dt, TZ.SYSTEM);
	}

	public static Date floor(Date dt, TZ tz) {
		Calendar CAL = Calendar.getInstance();
		CAL.setTime(dt);
		CAL.setTimeZone(tz.tz);
		CAL.set(year(dt, tz), month(dt, tz), day(dt, tz), 0, 0, 0);
		return CAL.getTime();
	}

	public static boolean sameDay(Date dt1, Date dt2) {
		return ((year(dt1) == year(dt2)) && (month(dt1) == month(dt2)) && (day(dt1) == day(dt2)));
	}

	public static enum TZ {
		GMT("GMT"),
		/* list of UTC offsets: http://en.wikipedia.org/wiki/List_of_time_zone_abbreviations */
		// new york DAYLIGHT TIME
		NEW_YORK("GMT-04"),
		// new york STANDARD TIME
		NEW_YORK_STANDARD_TIME("GMT-5"),
		SYSTEM(null);
		
		private final TimeZone tz;
		
		TZ(String label) {
			if(label == null) {
				tz = SYSTEM_TIMEZONE;
				return;
			}
			tz = TimeZone.getTimeZone(label);
		}
	}
}
