package com.threealike.life.core;

public interface ServiceStopListener {
	public void handleShutdown();
}
