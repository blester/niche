package com.threealike.life.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RequestEvent {
	
	final String serviceLabel;
	final Socket socket;
	
	RequestEvent(String serviceLabel, Socket socket) {
		this.serviceLabel = serviceLabel;
		this.socket = socket;
	}
	
	public final String getServiceLabel() {
		return this.serviceLabel;
	}
	
	public final Socket getSocket() {
		return this.socket;
	}
	
	/**
	 * Convenience method, eliminating the need to catch java.io.IOExceptions 
	 * all over the place
	 */
	public final InputStream socketGetInputStream() {
		
		try {
			return socket.getInputStream();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
		
	}
	
//	public final OutputStream socketGetOutputStream() {
//		try {
//			return socket.getOutputStream();
//		} catch (IOException e) {
//			throw new com.threealike.life.io.IOException(e);
//		}
//	}
	
//	public final void socketInputStreamClose() {
//		try {
//			socket.getInputStream().close();
//		} catch (IOException e) {
//			throw new com.threealike.life.io.IOException(e);
//		}
//	}
//	
//	public final void socketOutputStreamClose() {
//		try {
//			socket.getOutputStream().close();
//		} catch (IOException e) {
//			throw new com.threealike.life.io.IOException(e);
//		}
//	}
	
	public final OutputStream socketGetOutputStream() {
		try {
			return socket.getOutputStream();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public final void socketOutputStreamFlush() {
		try {
			socket.getOutputStream().flush();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
		
	}
	
	public final void socketClose() {
		try {
			socket.close();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
}
