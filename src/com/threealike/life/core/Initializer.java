package com.threealike.life.core;

public interface Initializer {
	
	public void init();
	
}
