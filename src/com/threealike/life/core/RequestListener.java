package com.threealike.life.core;

public interface RequestListener {
	public void handleRequest(RequestEvent agent);
}
