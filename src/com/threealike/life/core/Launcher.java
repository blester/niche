package com.threealike.life.core;

import com.threealike.life.reflection.Classes;
import com.threealike.life.thirdparty.org.json.JSONArray;
import com.threealike.life.thirdparty.org.json.JSONObject;


public class Launcher
{
	static 
	{
		final String adapterCname = Env.getConfig().has("niche-adapter") ? 
				Env.getConfig().getString("niche-adapter") : NicheAdapter.class.getName();
		NicheAdapter na = (NicheAdapter)Classes.newInstance(
				Classes.forName(adapterCname));
		System.setOut(na.getSysOutProvider());
		System.setErr(na.getSysErrorProvider());
		
		JSONObject conf = Env.getConfig();
		if(conf.has("apps")) {
			JSONArray apps = conf.getJSONArray("apps");
			for(int i=0; i < apps.length(); i++) {
				JSONObject app = apps.getJSONObject(i);
				if(app.has("initializer")) {
					String initerClassName = app.getString("initializer");
					Initializer initer = (Initializer)Classes.newInstance(Classes.forName(initerClassName));
					initer.init();
				}
			}
		}
		
		JSONArray services = Env.getConfig().getJSONArray("services");
		for(int i=0; i < services.length(); i++) {
			JSONObject service = services.getJSONObject(i);
			String label = service.getString("label");
			int port = service.getInt("port");
			int timeout = service.getInt("timeout");
			NicheService s = new NicheService(label, port, timeout);
			Env.SERVICES.put(label, s);
			s.start();
		}
	}
	public static void main(String[] args)
	{
		System.out.println("Niche is running...");
	}
}

