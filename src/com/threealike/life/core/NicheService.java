package com.threealike.life.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Iterator;

class NicheService extends java.lang.Thread {
	
	private final String label;
	private final int timeout;
	private final ServerSocket ss;
	
	NicheService(String label, int port, int timeout) {
		this.label = label;
		this.timeout = timeout;
		this.ss = nitSocket(port);
	}
	
	final void shutdown() {
		try {
			this.ss.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static final ServerSocket nitSocket(int port) {
		try {
			return new ServerSocket(port);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void run() {
		
		try {
			
			while(true) {
				
				Socket sckt = ss.accept();
				sckt.setSoTimeout(timeout);
				sckt.setTcpNoDelay(true);
				
				for(Iterator<RequestListener> it = Env.getRequestListeners(label).iterator(); it.hasNext();) {
					it.next().handleRequest(new RequestEvent(label, sckt));
				}
				
			}
			
		} catch(SocketException e) {
			//
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
