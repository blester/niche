package com.threealike.life.io;


/**
 * An IO Exception for lazy (virtuous) programmers
 * 
 * @author bretlester
 *
 */

@SuppressWarnings("serial")
public class IOException extends RuntimeException {

	public IOException(java.io.IOException cause) {
		super(cause);
	}

}
