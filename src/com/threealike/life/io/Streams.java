package com.threealike.life.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.threealike.life.util.Files;

public class Streams {

	public static final void transfer(InputStream is, File dest) {
		final FileOutputStream fos = Files.fileOS(dest);
		transfer(is, fos);
		Files.closeOS(fos);
	}
	
	public static final void transfer(InputStream is, OutputStream os) 
	{
		
		try {
			int len;
			byte[] buf = new byte[1024];
			while((len = is.read(buf)) != -1) {
				os.write(buf, 0, len);
			}
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
		
	}
	
	public static final void close(InputStream is) {
		try {
			is.close();
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static final OutputStream write(OutputStream os, String str) {
		return write(os, str.getBytes());
	}
	
	public static final OutputStream write(OutputStream os, byte[] buf) {
		return write(os, buf, 0, buf.length);
	}
	
	public static final OutputStream write(OutputStream os, byte[] buf, int offset, int len) {
		try {
			os.write(buf, offset, len);
			return os;
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static final int read(InputStream is, byte[] buf) {
		try {
			return is.read(buf);
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}

	
}
