package com.threealike.life;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

final public class Cmd
{
	public static final Results exec(String[] cmd) 
	{	
		Process proc = null;
		Runtime rt = Runtime.getRuntime();
		try {
			proc = rt.exec(cmd);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return runCommand(proc);
	}
    public static final Results exec(String cmd)
    {
    	Process proc = null;        
        Runtime rt = Runtime.getRuntime();
        try {
			proc = rt.exec(cmd);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
        return runCommand(proc);
    }
    public static final class Results {
    	public String out;
    	public String err;
    	public int exitVal;
    	private StringBuffer stdOut = new StringBuffer();
    	private StringBuffer stdErr = new StringBuffer();
    	private Results(){}
    	public final boolean hasOut() {
    		if(out == null || out.equals("")) return false;
    		return true;
    	}
    	public final boolean hasErr() {
    		if(err == null || err.equals("")) return false;
    		return true;
    	}
    }
    private static final Results runCommand(Process proc) 
    {
    	Results r = new Results();
	    StreamEater out = new StreamEater(proc.getInputStream(),r.stdOut);
	    StreamEater err = new StreamEater(proc.getErrorStream(),r.stdErr);
	    
	    out.start();
	    err.start();
	    
	    try {
		    while(out.eos != true || err.eos != true) {
		    	Thread.sleep(10);
		    }
			r.exitVal = proc.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

	    r.out = r.stdOut.toString();
	    r.err = r.stdErr.toString();
	    
	    return r;
    }
}
class StreamEater extends Thread 
{
	InputStream is;
	StringBuffer results;
	boolean eos = false;
	StreamEater(InputStream is, StringBuffer r)
	{
		this.is = is;
		this.results = r;
	}
	public void run()
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		int ch;
		try {
			while((ch=br.read()) != -1) {
				results.append((char)ch);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			eos = true;
		}
	}
}
