package com.threealike.life.file;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * converts array of command line args into simple 2-d object
 * following simple conventions
 * 
 *  example:
 *  
 *  -parent1 child11 child12 -parent2 child21 child22
 *  
 *  is converted to HashMap<parentx,childx[]>
 *
 */
public class CmdArgs {
	
	public static final String toString(Map<String, String[]> args) {
		String out = "";
		for(Iterator<String> it = args.keySet().iterator(); it.hasNext();) {
			String next = it.next();
			out += next+" = [";
			String[] arr = args.get(next);
			for(int i=0; i < arr.length; i++) {
				out += arr[i];
				if(i < arr.length-1) out += ", ";
			}
			out += "]\n";
		}
		return out;
	}
	
	public static final Map<String,String[]> parse(String[] args) {
		if(args == null) return null;
		HashMap<String,String[]> out = null;
		
		int i=0;
		String node;
		while(i < args.length) {
			if((node=isBasenode(args[i])) != null) {
				int j = i+1;
				while(j < args.length && isBasenode(args[j])==null) {
					j++;
				}
				String[] subargs = null;
				if(j-(i+1) > 0) {
					subargs = new String[j-(i+1)];
					j = i+1;
					while(j < args.length && isBasenode(args[j])==null) {
						subargs[j-(i+1)] = args[j];
						j++;
					}
				}
				i =j;
				if(out==null)
					out = new HashMap<String,String[]>();
				out.put(node, subargs);
				continue;
			}
			i++;
		}
		
		return out;
	}
	private static final String isBasenode(String node) {
		if(node.startsWith("-"))
			return node.substring(1);
		return null;
	}
}
