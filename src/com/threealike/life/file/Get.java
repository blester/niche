package com.threealike.life.file;

import java.io.File;
import java.io.FileInputStream;

public class Get 
{
	public static final byte[] contents(String filename) {
		return contents(new File(filename));
	}
	public static final byte[] contents(File f)
	{
		if(f==null) return null;
		byte[] out;
		try {
			FileInputStream fi = new FileInputStream(f);
			out = new byte[fi.available()];
			fi.read(out);
			fi.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return out;
	}
}
