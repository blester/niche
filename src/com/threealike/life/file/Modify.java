package com.threealike.life.file;

import java.io.File;

public class Modify {
	
	public static final boolean delete(File f) {
		if(f.isDirectory())
			return deleteDir(f);
		return f.delete();
	}
	
	public static final boolean deleteDir(File dir) {
		if(!dir.isDirectory()) return false;
		File[] files = dir.listFiles();
		for(File f : files) {
			if(f.isDirectory()) {
				deleteDir(f);
			} else {
				f.delete();
			}
		}
		return dir.delete();
	}
}
