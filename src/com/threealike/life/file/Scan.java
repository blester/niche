package com.threealike.life.file;

import java.io.File;

public class Scan {
	
	private static final boolean isDir(File f) {
		if(f == null) return false;
		return f.isDirectory();
	}
	
	public static final void scan(File dir, Examiner examiner) {
		if(!dir.isDirectory()) 
			return;
		File[] files = dir.listFiles();
		for(File f : files) {
			if(isDir(examiner.examine(f))) {
				scan(f, examiner);
			}
		}
	}
	
	
	public static interface Examiner {
		public File examine(File f);
	}
}
