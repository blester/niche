package com.threealike.life.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Highly modified source from http://www.javaworld.com/javaworld/javatips/javatip70/JarResources.java 
 */
public final class ZipReader {

	public static final void unzip(String jarFile) {
		unzip(new File(jarFile));
	}
	
	public static final void unzip(File jarFile) {
		
		File dest = new File(
			jarFile.getParent()+"/"+jarFile.getName().replaceAll("^(.*)\\.zip$", "$1"));
		dest.mkdir();
		
		unzip(jarFile, dest);
		
	}
	
	public static final void unzip(File jarFile, File dest) {
		
		String destString = dest.getAbsolutePath();
		List<ZipEntry> contents = listContents(jarFile);
		for(Iterator<ZipEntry> it = contents.iterator(); it.hasNext();) {
			String next = it.next().getName();
			File nextFile = new File(destString+"/"+next);
			File parentDir = nextFile.getParentFile();
			parentDir.mkdirs();
			try {
				
				nextFile.createNewFile();
				
				OutputStream os = new FileOutputStream(nextFile);
				byte[] data = getResource(jarFile, next);
				if(data != null)
					os.write(data);
				os.close();
				
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
	/**
	 * Use this in confunction w/listContents to unzip a file
	 * 
	 * @param jarFile
	 * @param name
	 * @return
	 */
	public static final byte[] getResource(File jarFile, String name) {
		try {
			int size = 0;
			// extracts just sizes only.
			ZipFile zf = new ZipFile(jarFile);
			Enumeration<? extends ZipEntry> e = zf.entries();
			while (e.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) e.nextElement();

				if(ze.getName().equals(name))
					size = (int)ze.getSize();
			}
			zf.close();
			
			if(size == 0) return null;

			// extract resources and put them into the hashtable.
			FileInputStream fis = new FileInputStream(jarFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			ZipInputStream zis = new ZipInputStream(bis);
			ZipEntry ze = null;
			while ((ze = zis.getNextEntry()) != null) {
				if (ze.isDirectory()) {
					continue;
				}
				if(!ze.getName().equals(name)) {
					continue;
				}

				byte[] b = new byte[size];
				int rb = 0;
				int chunk = 0;
				while (((int) size - rb) > 0) {
					chunk = zis.read(b, rb, size - rb);
					if (chunk == -1) {
						break;
					}
					rb += chunk;
				}

				return b;
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static final List<ZipEntry>	listContents(File jarFile) {
		List<ZipEntry> out = new ArrayList<ZipEntry>();
		try {
			FileInputStream fis = new FileInputStream(jarFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			ZipInputStream zis = new ZipInputStream(bis);
			ZipEntry ze = null;
			while((ze = zis.getNextEntry()) != null) {
				out.add(ze);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}

}
