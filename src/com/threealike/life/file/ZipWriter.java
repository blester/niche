package com.threealike.life.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * simple class for creating .zip archives
 * thank you http://www.acm.org/crossroads/xrds6-3/ovp63.html
 */
public class ZipWriter {
	
	private ZipWriter() {}
	
//	/**
//	 * Simple command line zip utility. 
//	 * 
//	 * Arguments: source destination [-ex [exclusion_pattern1, [exclusion_pattern2, [...]]]
//	 * 
//	 * @param args
//	 * @throws IOException
//	 */
//	public static void main(String[] args) throws IOException {
//		
//		String source = args[0];
//		String destination = args[1];
//		
//		if(source == null) throw new RuntimeException("Must specify source as first arg");
//		
//		if(destination == null) destination = source+".zip";
//		
//		HashMap<String, String[]> ex = CmdArgs.parse(args);
//		Examiner exam = null;
//		if(ex != null && ex.get("ex") != null) {
//			final String[] exPatterns = ex.get("ex");
//			exam = new Examiner() {
//				
//				public boolean include(File file) {
//					for(String pat : exPatterns) {
//						if(file.getName().matches(pat)) 
//							return false;
//					}
//					return true;
//				}
//				
//			};
//		}
//		
//		File src = new File(source);
//		if(!src.exists()) 
//			throw new RuntimeException("Srouce File, \""+source+"\" does not exist.");
//		
//		System.out.println("zipping");
//		zip(new File(source), new File(destination), exam);
//		System.out.println("Done");
//	}
	
	/**
	 * converts the file argument to a zip archive of the same name 
	 * in the same directory
	 */
	public static final void zip(File source) 
	{
		zip(source, new File(source.getAbsolutePath() + ".zip"));
	}
	
	public static final void zip(File source, File destination) 
	{
		zip(source, destination, null);
	}
	
	public static final void zip(File source, File destination, Examiner examiner) 
	{
		try {
			
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(destination));
			zip(source, zos, examiner);
			zos.close();
			
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}
	}
	
	public static final void zip(File source, ZipOutputStream destination) {
		zip(source, destination, null);
	}
	
	public static final void zip(File source, ZipOutputStream destination, Examiner examiner) {
		try {

			recurseFiles(source.getAbsoluteFile(), destination, source, examiner);
			
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}	
	}
	
	public static final void zip(List<File> sources, File destination) {
		zip(sources, destination, null);
	}
	
	public static final void zip(List<File> sources, File destination, Examiner examiner) {
		File[] ar = new File[sources.size()];
		for(int i=0; i < sources.size(); i++) {
			ar[i] = sources.get(i);
		}
		zip(ar, destination, examiner);
	}
	
	public static final void zip(File[] sources, File destination) {
		zip(sources, destination, null);
	}
	
	public static final void zip(File[] sources, File destination, Examiner examiner)
	{
		try {
			
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(destination));

			//recurseFiles(source.getAbsoluteFile(), zos, source, examiner);
			for(File source : sources) {
				recurseFiles(source.getAbsoluteFile(), zos, source, examiner);
			}

			zos.close();
			
		} catch (IOException e) {
			throw new com.threealike.life.io.IOException(e);
		}	
	}

	private static void recurseFiles(File bse, ZipOutputStream zos, File file, Examiner examiner)
			throws IOException, FileNotFoundException 
	{
		if(examiner != null && (!examiner.include(file))) return;
		
		if (file.isDirectory()) 
		{
			//Create an array with all of the files and subdirectories
			//of the current directory.
			String[] fileNames = file.list();
			if (fileNames != null) 
			{
				//Recursively add each array entry to make sure that we get
				//subdirectories as well as normal files in the directory.
				for (int i = 0; i < fileNames.length; i++) 
				{
					recurseFiles(bse, zos, new File(file, fileNames[i]), examiner);
				}
			}
		}
		//Otherwise, a file so add it as an entry to the Zip file.
		else 
		{
			addZipEntry(bse, file, zos);
		}
	}
	
	private static final void addZipEntry(File bse, File file, ZipOutputStream zos) 
		throws IOException 
	{
		byte[] buf = new byte[1024];
		int len;
		//Create a new Zip entry with the file's name.
		//NEEDS TO BE A RELATIVE PATH TO PARENT DIRECTORY
		String entryLabel = file.getAbsolutePath().substring(bse.getParentFile().getAbsolutePath().length()+1);
		entryLabel = entryLabel.replace(File.separatorChar, '/');
		//System.out.println(entryLabel);
		ZipEntry zipEntry = new ZipEntry(entryLabel);
		//Create a buffered input stream out of the file
		//we're trying to add into the Zip archive.
		FileInputStream fin = new FileInputStream(file);
		BufferedInputStream in = new BufferedInputStream(fin);
		zos.putNextEntry(zipEntry);
		//Read bytes from the file and write into the Zip archive.
		while ((len = in.read(buf)) >= 0) 
		{
			zos.write(buf, 0, len);
		}
		//Close the input stream.
		in.close();
		//Close this entry in the Zip stream.
		zos.closeEntry();
	}
	
	public static interface Examiner {
		public boolean include(File f);
	}
}
