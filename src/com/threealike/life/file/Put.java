package com.threealike.life.file;

import java.io.File;
import java.io.FileOutputStream;

public class Put 
{
	public static void contents(String filename, String contents)
	{
		contents(new File(filename),contents);
	}
	public static void contents(File f, StringBuffer sb)
	{
		contents(f,sb.toString());
	}
	public static void contents(File f, String contents)
	{
		contents(f, contents.getBytes());
	}
	public static void contents(File f, byte[] contents)
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream(f,false);
			fos.write(contents);
			fos.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
